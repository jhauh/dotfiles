dotfiles
========

A collection of my excessively curated configuration files. Uses 
`GNU Stow <http://www.gnu.org/software/stow/>`_ to manage symlinks so
you don't have to.

Installation
------------

This repo contains submodules. To clone them along with the rest of the
repo, try::

    git clone --recursive https://gitlab.com/jhauh/dotfiles.git

Or if you're running git 2.13 or later, you can fetch them in parallel
with::

    git clone --recurse-submodules -j4  https://gitlab.com/jhauh/dotfiles.git

Note that this only matters if you want the Vim configs, if not then you
can clone normally.

Usage
-----

From inside the dotfiles folder, run::

    stow vim
    stow tmux
    ...
    etc.

..To pick and choose which config files you want to try out. To undo any
of these, run::

    stow -D vim
    ...
    etc.

General principals
------------------

- Keep configs out of ``$HOME`` where possible
- Prefer modular to monoliths
- Stock look & feel unless there's a good reason not to
- Comments help future you

Loose config groupings
----------------------

Some programs and their configs are built to work together, so if you
want one of the configs in these groups, consider the others in that
group as well:

**Sway**: ``sway``, ``waybar``, ``mako``, ``kanshi``, ``swaylock``

**i3**: ``i3``, ``dunst``, ``compton``

**MacOS**: ``yabai``, ``skhd``

There are also a slew of terminal/command line programs which can be
used independently anywhere:

**Terminal**: ``bash``, ``fish``, ``git``, ``vim``, ``tmux``,
``ranger``...

Individual package notes
------------------------

Bash
****

``.bashrc`` is modular, so leave it alone and instead add your configs
to ``~/.config/bashrc.d/``. 
`Reference <https://sneak.berlin/20191011/stupid-unix-tricks/>`_.

Vim
***

Also modular. Plugins are handled with Vim's builtin package manager
(8.0+), and are included in this repo as submodules. For tidiness,
``vimrc`` lives inside of ``.vim``, which will work with
versions >=7.4. If you're stuck with an earlier version, simply move it
to ``~/.vimrc`` instead.

Git
***

Older versions of git may not check the current folder location. If so,
copy ``config`` to ``~/.gitconfig``. A global ``user.name`` and
``user.email`` are purposely not set; specify these locally depending on
which account you are committing from (i.e. GitLab/GitHub/SourceHut/work
etc.).

Tmux
****

Uses a nice check to work around different ``tmux`` versions.
`Reference <https://stackoverflow.com/a/40902312>`_.

i3
**

Nobody runs plain i3 anymore, so install 
`i3-gaps <https://github.com/Airblader/i3>`_. If you do run vanilla i3,
try the configs and remove any lines it complains about. Main config
file could do with some love but there are still some nuggets in there
(e.g. ``i3_rename_workspaces.sh``).

Sway
****

Modular. Notable snippets include an equivalent ``rename_workspace.sh``
and a Wayland-native dmenu-like application launcher.

Yabai & skhd
************

Highly linked, so run both together. 
`Stackline <https://github.com/AdamWagner/stackline>`_ adds some visual
flare but isn't necessary. Might complain about some options if SIP is
enabled so just remove them.

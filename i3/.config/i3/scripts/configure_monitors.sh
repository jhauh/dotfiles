#!/bin/bash

# Check to see if other monitors are connected
n_monitors=$( xrandr --listmonitors | awk 'NR==1{ print $2 }' )
echo $n_monitors
if [ $n_monitors -gt 1 ]
then
    bash ~/.config/i3/scripts/layouts/4koverlaptop.sh
fi


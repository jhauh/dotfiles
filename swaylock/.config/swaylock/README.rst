Swaylock config notes
=====================

Not sure how to add comments to the config file so this will do.

Assumes use of 
`swaylock-effects <https://github.com/mortie/swaylock-effects>`_
rather than vanilla swaylock. Options common to both are at the top of
the config file, if you're using vanilla simply cut the options at the
bottom.

The blur effects are nice, but pixelate is faster to run. Value should
be set equal to waybar height for maximum aesthetic.

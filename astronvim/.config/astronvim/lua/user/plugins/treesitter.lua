return {
  "nvim-treesitter/nvim-treesitter",
  build = function()
      require("nvim-treesitter.install").update({ with_sync = true })
    end,
  dependencies = {
    "nvim-treesitter/nvim-treesitter-textobjects",
    "nvim-treesitter/playground",
    {
      "andymass/vim-matchup",
      init = function() vim.g.matchup_matchparen_deferred = 1 end,
    },
  },
  opts = {
    highlight = {
      enable = true,
      additional_vim_regex_highlighting = { "org" },
      disable = { "help" },
    },
    indent = {
      enable = true,
    },
    matchup = {
      enable = true,
    },
    textobjects = {
      select = {
        enable = true,
        -- Automatically jump forward to textobj, like targets.vim
        lookahead = true,
        keymaps = {
          -- Capture groups defined in textobjects.scm
          ["af"] = "@function.outer",
          ["if"] = "@function.inner",

          ["ac"] = "@call.outer",
          ["ic"] = "@call.inner",

          ["aC"] = "@class.outer",
          ["iC"] = "@class.inner",

          ["aa"] = "@parameter.outer",
          ["ia"] = "@parameter.inner",

          ["a/"] = "@comment.outer",
          ["i/"] = "@comment.outer",

          ["al"] = "@loop.outer",
          ["il"] = "@loop.inner",

          ["ab"] = "@block.outer",
          ["ib"] = "@block.inner",

          ["aS"] = "@statement.outer",
          ["iS"] = "@statement.inner",

          ["ai"] = "@conditional.outer",
          ["ii"] = "@conditional.inner",
        },
      },
    },
    move = {
      enable = true,
      set_jumps = true,
      goto_next_start = {
        ["]b"] = { query = "@block.outer", desc = "Next block start" },
        ["]f"] = { query = "@function.outer", desc = "Next function start" },
        ["]a"] = { query = "@parameter.outer", desc = "Next parameter start" },
        ["]C"] = { query = "@class.outer", desc = "Next class start" },
        ["]/"] = { query = "@comment.outer", desc = "Next comment start" },
      },
      goto_next_end = {
        ["]B"] = { query = "@block.outer", desc = "Next block end" },
        ["]F"] = { query = "@function.outer", desc = "Next function end" },
        ["]A"] = { query = "@parameter.outer", desc = "Next parameter end" },
      },
      goto_previous_start = {
        ["[b"] = { query = "@block.outer", desc = "Previous block start" },
        ["[f"] = { query = "@function.outer", desc = "Previous function start" },
        ["[a"] = { query = "@parameter.outer", desc = "Previous parameter start" },
        ["[C"] = { query = "@class.outer", desc = "Previous class start" },
        ["[/"] = { query = "@comment.outer", desc = "Previous comment start" },
      },
      goto_previous_end = {
        ["[B"] = { query = "@block.outer", desc = "Previous block end" },
        ["[F"] = { query = "@function.outer", desc = "Previous function end" },
        ["[P"] = { query = "@parameter.outer", desc = "Previous parameter end" },
      },
    },
    swap = {
      enable = true,
      swap_next = {
        [">B"] = { query = "@block.outer", desc = "Swap next block" },
        [">F"] = { query = "@function.outer", desc = "Swap next function" },
        [">A"] = { query = "@parameter.inner", desc = "Swap next parameter" },
      },
      swap_previous = {
        ["<B"] = { query = "@block.outer", desc = "Swap previous block" },
        ["<F"] = { query = "@function.outer", desc = "Swap previous function" },
        ["<A"] = { query = "@parameter.inner", desc = "Swap previous parameter" },
      },
    },
    lsp_interop = {
      enable = true,
      border = "single",
      peek_definition_code = {
        ["<leader>lp"] = { query = "@function.outer", desc = "Peek function definition" },
        ["<leader>lP"] = { query = "@class.outer", desc = "Peek class definition" },
      },
    },
    incremental_selection = {
      enable = true,
      keymaps = {
        init_selection = "<c-space>",
        node_incremental = "<c-space>",
        scope_incremental = "<c-s>",
        node_decremental = "<M-space>",
      }
    },
  }
}



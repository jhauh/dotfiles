return {
  "chipsenkbeil/distant.nvim",
  cmd = "DistantLaunch",
  config = function()
    require("distant").setup {
      -- Merges the default settings, mappings etc. with default ssh mode 
      ['*'] = vim.tbl_deep_extend(
        'keep',
        require('distant.settings').chip_default(),
        {
          mode = 'ssh'
        }
      ),
    }
  end
}

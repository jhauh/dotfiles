-- Neorg config
return {
  -- Tell Neorg what modules to load
  "nvim-neorg/neorg",
  ft = "norg",
  build = ":Neorg sync-parsers",
  cmd = 'Neorg',
  dependencies = "nvim-lua/plenary.nvim",
  opts = {
    load = {
      ["core.defaults"] = {}, -- Load all the default modules
      ["core.keybinds"] = { -- Configure core.keybinds
        config = {
          default_keybinds = true, -- Generate the default keybinds
          neorg_leader = "<Leader>", -- This is the default if unspecified
        },
      },
      ["core.concealer"] = {}, -- Allows for use of icons
      ["core.dirman"] = { -- Manage your directories with Neorg
        config = {
          workspaces = {
            my_workspace = "~/neorg",
          },
        },
      },
      ["core.completion"] = {
        config = {
          engine = "nvim-cmp",
        },
      },
    },
  }
}

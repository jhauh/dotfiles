return {
  {
    "folke/zen-mode.nvim",
    cmd = "ZenMode",
    keys = {
      { "<leader>zz", ":ZenMode<cr>", desc = "Zen Mode" },
    },
    opts = {
      window = {
        backdrop = 0.999,
        height = 0.9,
        width = 140,
        options = {
          number = false,
          relativenumber = false,
        },
      },
      plugins = {
        kitty = {
          enabled = true,
        },
      },
    },
  },
  {
    "folke/twilight.nvim",
    opts = {
      context = -1
    },
  }
}

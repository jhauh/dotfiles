-- Mapping data with "desc" stored directly by vim.keymap.set().
--
-- Please use this mappings table to set keyboard mapping since this is the
-- lower level configuration and more robust one. (which-key will
-- automatically pick-up stored data by this setting.)
local function toggle_colorcolumn()
    if #vim.opt_local.colorcolumn:get() > 0 then
        vim.opt_local.colorcolumn = {}
    else
        vim.opt_local.colorcolumn = { 72, 79 }
    end
end


return {
  -- first key is the mode
  n = {
    -- second key is the lefthand side of the map
   -- mappings seen under group name "Buffer"
    ["<leader>bD"] = {
      function()
        require("astronvim.utils.status").heirline.buffer_picker(
          function(bufnr) require("astronvim.utils.buffer").close(bufnr) end
        )
      end,
      desc = "Pick to close",
    },
    ["<leader>c"] = false,
    ["<leader>C"] = false,
    ["yoc"] = { toggle_colorcolumn, desc = "Toggle colorcolumns" },
    ["[oc"] = {  function() vim.opt_local.colorcolumn = { 72, 79 } end },
    ["]oc"] = {  function() vim.opt_local.colorcolumn = {} end },
    ["<C-S-j>"] = "<cmd>m .+1<CR>==",
    ["<C-S-k>"] = "<cmd>m .-2<CR>==",
    ["<C-S-Down>"] = "<cmd>m .+1<CR>==",
    ["<C-S-Up>"] = "<cmd>m .-2<CR>==",
    ["<leader>x"] = { name = "裂Trouble" },
    ["<leader>z"] = { name = "Zen" },
  },
  v = {
    ["jk"] = "<Esc>",
    ["jj"] = nil,
    [">"] = ">gv",
    ["<"] = "<gv",
    ["<C-S-j>"] = ":m '>+1<CR>gv=gv",
    ["<C-S-k>"] = ":m '<-2<CR>gv=gv",
    ["<C-S-Down>"] = ":m '>+1<CR>gv=gv",
    ["<C-S-Up>"] = ":m '<-2<CR>gv=gv",
  },
}

return {
    { "tpope/vim-repeat", event = "BufReadPre" },
    { "tpope/vim-unimpaired", event = "BufReadPre" },
    { "tpope/vim-sleuth", event = "BufReadPre" },
    {
    "tpope/vim-fugitive",
    cmd = { "Git", "Gstatus", "Gblame", "Gpush", "Gpull" }
    },
}
return {
  "hrsh7th/nvim-cmp",
  dependencies = {
    "hrsh7th/cmp-nvim-lsp", -- source for neovim builtin LSP client
    { "hrsh7th/cmp-nvim-lua", ft = "lua" }, -- source for nvim lua
    "hrsh7th/cmp-buffer", -- source for buffer words.
    "hrsh7th/cmp-path", -- source for filesystem paths.
    "hrsh7th/cmp-cmdline", -- source for vim command line.
    "saadparwaiz1/cmp_luasnip", -- luasnip completion source
  },
  keys = { ":", "/", "?" },
  config = function(_, opts)
    local cmp = require("cmp")
    opts.sources = cmp.config.sources {
      { name = "nvim_lsp" },
      { name = "nvim_lsp_signature_help" },
      { name = "buffer" },
      { name = "luasnip" }, -- For luasnip users.
      { name = "neorg" },
    }
    opts.experimental = {
      ghost_text = true,
    }

    cmp.setup(opts)

    cmp.setup.cmdline("/", {
      mapping = cmp.mapping.preset.cmdline(),
      sources = {
        { name = "buffer", keyword_length = 3 },
      },
    })

    cmp.setup.cmdline(":", {
      mapping = cmp.mapping.preset.cmdline(),
      sources = cmp.config.sources({
        { name = "path", keyword_length = 3 },
      }, {
        { name = "cmdline", keyword_length = 2 },
      }),
    })
  end
}

#!/bin/bash

ws=$(exec ~/.config/sway/scripts/sway_get_workspace.sh)
command="~/.config/sway/scripts/sway-input.sh -F 'rename workspace to \"$(echo $ws): %s\"' -P 'New name for this workspace: $(echo $ws): '"

eval $(echo $command)

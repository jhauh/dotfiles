#!/usr/bin/env bash

# History options 
HISTSIZE=1000
HISTFILESIZE=2000
# Don't put duplicate lines or lines starting with space in the history
HISTCONTROL=ignoreboth
# Append to the history file, don't overwrite it
shopt -s histappend
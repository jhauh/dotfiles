" Load additional plugins and configure their settings

" Plugin settings

" Built-in file browser settings
let g:netrw_liststyle = 3			" Tree-style
let g:netrw_browse_split = 4  		" Open files in the main window
"let g:netrw_altv = 1				" Open on left
let g:netrw_winsize = 25			" Quarter of window
let g:netrw_banner = 0				" Hide banner
"let g:netrw_list_hide = &wildignore " Ignore unvimable files
let g:netrw_dirhistmax = 0			" Dont save dir history

" Send code to tmux ipython panes
let g:slime_target = "tmux"
let g:slime_python_ipython = 1

" Git gutter
let g:signify_sign_change = '>'
let g:signify_sign_show_count = 0
set updatetime=1000

" ALE language server/syntax checker
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_sign_error = '>'
let g:ale_sign_warning = '-'
let g:ale_lint_delay = 500
let g:ale_linters = {'python': ['pyls', 'flake8', 'pylint']}
let g:ale_completion_enabled = 1
set omnifunc=ale#completion#OmniFunc
highlight ALEWarning ctermbg=DarkGrey
highlight ALEError ctermbg=DarkRed
"let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'


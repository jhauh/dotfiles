" Generic behavioural settings

" Use vim settings rather than vi settings (must be first)
set nocompatible

" Lots of filetype-specific stuff
filetype plugin indent on

" Hide buffers, dont close them
set hidden

" Dont allow files to set their own settings (for security)
set modelines=0

" Fuzzy find (controversial)
set path+=**

" Better command line completion
set wildmode=longest,list,full
set wildmenu
set wildignorecase

" ..But ignore files vim doesnt use - helps with fuzzy find
set wildignore+=.git,.hg,.svn
set wildignore+=*.aux,*.out,*.toc
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest,*.rbc,*.class
set wildignore+=*.ai,*.bmp,*.gif,*.ico,*.jpg,*.jpeg,*.png,*.psd,*.webp
set wildignore+=*.avi,*.divx,*.mp4,*.webm,*.mov,*.m2ts,*.mkv,*.vob,*.mpg,*.mpeg
set wildignore+=*.mp3,*.oga,*.ogg,*.wav,*.flac
set wildignore+=*.eot,*.otf,*.ttf,*.woff
set wildignore+=*.doc,*.docx,*.pdf,*.cbr,*.cbz
set wildignore+=*.zip,*.tar.gz,*.tar.bz2,*.rar,*.tar.xz,*.kgb
set wildignore+=*.swp,*.swo,.lock,.DS_Store
set wildignore+=*.pyc

" Case insensitive search, except when using capital letters
set ignorecase
set smartcase

" Highlight searches, and start searching as you start typing
set hlsearch
set incsearch

" Allow backspace over autoindent, line breaks and start of insert action
set backspace=indent,eol,start

" When opening a new line and no filetype-specific indenting is enabled, keep
" the same indent as the line you're currently on. Useful for READMEs, etc.
set autoindent

" Stop certain movements from always going to the first character of a line.
set nostartofline

" Raise a dialogue asking if you wish to save changed files instead of 
" failing " a command because of unsaved changes 
set confirm

" Enable use of the mouse for all modes
set mouse=a

" Indentation settings for using 4 spaces instead of tabs.
" Do not change 'tabstop' from its default value of 8 with this setup.
set shiftwidth=4
set softtabstop=4
set expandtab

" Change behaviour of splits, keep original file on top/left
set splitbelow
set splitright

" Use system clipboard
set clipboard=unnamed

" Write viminfo inside .vim, not ~
set viminfo+=n~/.vim/.viminfo

" Make wrapping work correctly on lists
set formatoptions+=n

" Define lists so wrapping text doesnt mangle them
set formatlistpat=^\\s*                     " Optional leading whitespace
set formatlistpat+=[                        " Start character class
set formatlistpat+=\\[({]\\?                " |  Optionally match opening punctuation
set formatlistpat+=\\(                      " |  Start group
set formatlistpat+=[0-9]\\+                 " |  |  Numbers
set formatlistpat+=\\\|                     " |  |  or
set formatlistpat+=[a-zA-Z]\\+              " |  |  Letters
set formatlistpat+=\\)                      " |  End group
set formatlistpat+=[\\]:.)}                 " |  Closing punctuation
set formatlistpat+=]                        " End character class
set formatlistpat+=\\s\\+                   " One or more spaces
set formatlistpat+=\\\|                     " or
set formatlistpat+=^\\s*[-–+o*•]\\s\\+      " Bullet points

" Treat all numbers as decimal, for <C-a> & <C-x>
set nrformats=

" Reduce timeout for key sequences (mainly for jk in Visual mode)
set timeoutlen=700

" Add keyboard shortcuts

" Toggle the directory explorer
nnoremap <leader>e <esc>:Lexplore<esc>

" j-then-k quickly functions as escape from insert and visual
inoremap jk <Esc>
vnoremap jk <Esc>gV

" %% expands to folder of active buffer in command mode
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'

" Jump to next ALE complaint
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

" unimpaired-style toggles for plugins/settings
" ALE syntax checker/linter
nmap yot :ALEToggle<CR>
nmap [ot :ALEEnable<CR>
nmap ]ot :ALEDisable<CR>

" Signify git gitter 
nmap yog :SignifyToggle<CR>
nmap [og :SignifyEnable<CR>
nmap ]og :SignifyDisable<CR>

" Python column rulers - overwrites unimpaired cursorcolumn
nmap yoc :execute "set colorcolumn=" . (&colorcolumn == "" ? "72,79" : "")<CR>
nmap [oc :set colorcolumn=72,79<CR>
nmap ]oc :set colorcolumn=""<CR>


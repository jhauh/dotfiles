Practical Vim Tips
==================

Stuff I learned reading Practical Vim

Lower case::

  gu{motion}

Upper case::

  gU{motion}


Reverse case::

  g~{motion}


Comment (requires commentary.vim plugin)::

  gc{motion}



Delete back one word (in insert mode)::

  <C-w>


Delete back to start of line (in insert mode)::

  <C-u>


Insert normal mode (single normal command from insert mode)::

  <C-o>


Insert exotic characters from insert mode::

  <C-k>{digraph}


Replace mode (like insert mode but overwrites)::

  R


Reselect the last visual selection::

  gv


(In visual mode) go to other end of selected text::

  o


(In command/search mode) get the word under the cursor::

  <C-r><C-w>


Command line window (edit historical commands)::

  q:


Command line window (edit historical searches)::

  q/


When Vim won't let you write to a non-existant folder::

  :!mkdir -p %:h (or :!mkdir -p %%)


Transpose the next two characters::

  xp


Insert at last insert point::

  gi


Create mark::

  m{a-z}


Create global mark (persists)::

  m{A-Z}


Go to marked line::

  '{mark}


go to marked position::

  `{mark}


Paste contents of register (from insert mode)::

  <C-r>{register}


Paste contents of unnamed register (from insert mode)::

  <C-r>"


Paste contents of yank register (from insert mode)::

  <C-r>0


Motion to operate on the next and previous match::

  gn and gN


Repeat the previous substitution across the entire file::

  g&


(In command mode) repeat last substitution with same flags::

  &&


Recipes
-------

Underline a heading::

  yyp
  Vr-

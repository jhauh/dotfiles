" Settings which affect how vim looks

" Highlighting
syntax on

" Number column
set number

" Force the sign column so plugins don't pop jank it in
set signcolumn=yes

" Dont show eol markers (this affects indentguides)
set listchars-=eol:$
set listchars+=tab:\ \ 

" Rulers at python guides
"set colorcolumn=72,79

" Show partial commands in the last line of the screen
set showcmd

" Show matching brackets/parenthesis
set showmatch

" Display the cursor position in the status line of a window
set ruler

" Always display the status line, even if only one window is displayed
set laststatus=2

" Use visual bell but turn it off
set visualbell
set t_vb=

" Disable startup message
set shortmess+=I

color brogrammer  " Find a new one
